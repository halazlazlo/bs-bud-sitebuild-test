module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt)

  var configs = require('load-grunt-configs')(grunt);
  grunt.initConfig(configs);

  grunt.registerTask('default', [
    'sass',
    'postcss',
    'cmq',
    'watch'
  ]);
}
