module.exports = {
  default: {
    options: {
      sourceMap: false,
      outputStyle: 'compressed',
      includePaths: [
        'node_modules'
      ]
    },
    files: [{
      expand: true,
      cwd: 'scss',
      src: ['**/*.scss'],
      dest: '',
      ext: '.css'
    }]
  }
};
