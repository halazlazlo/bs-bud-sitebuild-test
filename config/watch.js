module.exports = {
  css: {
    files: [
      'scss/**/*'
    ],
    tasks: [
      'sass',
      'postcss',
      'cmq'
    ],
    options: {
      spawn: false
    }
  }
};
